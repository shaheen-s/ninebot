﻿using NineBot.Models;
using NineBot.Utils;
using System.Threading.Tasks;
using Discord.WebSocket;

namespace NineBot.Db_logic
{
    class Db_abstraction
    {
        public static async Task UpdateUser(User dbUser, SocketGuildUser user)
        {
            //Ignore botted accounts
            if (user.IsBot) return;

            //Add users with no roles
            if(user.Roles.Count == 0)
            {
                await DbLogic.AddUser(dbUser);
                return;
            }

            foreach (var role in user.Roles)
            {
                Role dbRole = Utilties.DiscordRoleToDbRole(role);
                await DbLogic.UpdateRoleUsers(dbUser, dbRole);
            }

            //If roles have been removed, remove them from db
            await DbLogic.RemoveRoleUsers(user, dbUser);

            //If name has changed, update it.
            await DbLogic.UpdateUserName(dbUser, user);
        }

        public static async Task UpdateRole(Role dbRole, SocketRole role)
        {
            await DbLogic.UpdateRolePermissions(dbRole, role.Permissions.ToList());
            await DbLogic.UpdateRoleInformation(dbRole, role);
        }
    }
}
