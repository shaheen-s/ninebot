﻿using Discord.WebSocket;
using Microsoft.EntityFrameworkCore;
using NineBot.DAL;
using NineBot.Interfaces;
using NineBot.Models;
using NineBot.Utils;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NineBot.Db_logic
{
    class DbLogic
    {

        public static async Task AddPermission(Permission permission)
        {
            using (NineBotContext dbContext = new NineBotContext())
            {
                bool foundPermission = dbContext.Permissions.Any(p => p.PermissionID == permission.PermissionID);
                if (foundPermission) return;

                await dbContext.Permissions.AddAsync(permission);
                await dbContext.SaveChangesAsync();
            }
        }

        public static async Task AddRole(Role role)
        {
            using (NineBotContext dbContext = new NineBotContext())
            {
                bool foundRole = dbContext.Roles.Any(r => r.RoleID == role.RoleID);
                if (foundRole) return;

                await dbContext.Roles.AddAsync(role);
                await dbContext.SaveChangesAsync();
            }
        }

        public static async Task UpdateRoleInformation(Role dbRole, SocketRole role)
        {
            using (NineBotContext dbContext = new NineBotContext())
            {
                dbRole = dbContext.Roles.FirstOrDefault(r => r.RoleID == role.Id);

                if (dbRole == null)
                {
                    //TODO: Throw exception
                    return;
                }

                Role discordRole = Utilties.DiscordRoleToDbRole(role);
                if (discordRole == dbRole) return;

                dbRole.Name = discordRole.Name;
                dbRole.Color = discordRole.Color;
                dbRole.DisplayedSeperately = discordRole.DisplayedSeperately;
                dbRole.MentionableByAll = discordRole.MentionableByAll;

                await dbContext.SaveChangesAsync();
            }
        }
        public static async Task UpdateRolePermissions(Role dbRole, List<Discord.GuildPermission> permissions)
        {
            using (NineBotContext dbContext = new NineBotContext())
            {
                bool foundRole = dbContext.Roles.Any(r => r.RoleID == dbRole.RoleID);
                if (!foundRole)
                {
                    //TODO: Throw exception
                    return;
                }

                if (permissions.Count == 0)
                {
                    await AddRole(dbRole);
                    return;
                }

                foreach (var permission in permissions)
                {
                    var dbPermission = Utilties.DiscordPermissionToDbPermission(permission);
                    bool foundPermission = dbContext.Permissions.Any(p => p.PermissionID == dbPermission.PermissionID);

                    dbRole = foundRole ? dbContext.Roles
                        .Include(r => r.RolePermissions)
                        .FirstOrDefault(r => r.RoleID == dbRole.RoleID)
                        : dbRole;

                    dbPermission = foundPermission ? dbContext.Permissions
                        .Include(p => p.RolePermissions)
                        .FirstOrDefault(p => p.PermissionID == dbPermission.PermissionID)
                        : dbPermission;

                    RolePermission rolePermission = new RolePermission
                    {
                        Role = dbRole,
                        Permission = dbPermission
                    };

                    bool foundRelationship = dbRole.RolePermissions.Any(rp => rp.PermissionId == dbPermission.PermissionID && rp.RoleId == dbRole.RoleID);
                    if (foundRelationship) continue;

                    dbRole.RolePermissions.Add(rolePermission);

                    await dbContext.SaveChangesAsync();
                }
            }
        }

        public static async Task AddUser(User user)
        {
            using (NineBotContext dbContext = new NineBotContext())
            {
                bool foundUser = dbContext.Users.Any(u => u.UserID == user.UserID);
                if (foundUser) return;

                await dbContext.Users.AddAsync(user);
                await dbContext.SaveChangesAsync();
            }
        }

        private static async Task AddRoleUser(IRoleOrUser roleOrUser, User user, Role role, NineBotContext dbContext, bool update)
        {
            //update existing record instead of adding new one
            if (update)
            {
                var roleUser = user.RoleUsers.FirstOrDefault(ru => ru.RoleId == role.RoleID && ru.UserId == user.UserID);

                if (roleUser != null) return;

                user.RoleUsers.Add(new RoleUser
                {
                    Role = role,
                    User = user
                });
            }
            else
            {
                roleOrUser.RoleUsers = new List<RoleUser>
                {
                    new RoleUser
                    {
                        Role = role,
                        User = user
                    }
                };

                await dbContext.AddAsync(roleOrUser);
            }
        }

        public static async Task UpdateUserName(User dbUser, SocketGuildUser user)
        {
            using (NineBotContext dbContext = new NineBotContext())
            {
                dbUser = dbContext.Users.FirstOrDefault(u => u.UserID == dbUser.UserID);

                if (dbUser == null)
                {
                    //TODO: Throw exception
                    return;
                }

                User discordUser = Utilties.DiscordUserToDbUser(user);

                if (discordUser == dbUser) return;

                dbUser.TagName = discordUser.TagName;
                dbUser.TagID = discordUser.TagID;

                await dbContext.SaveChangesAsync();
            }
        }

        public static async Task UpdateRoleUsers(User user, Role role)
        {
            using (NineBotContext dbContext = new NineBotContext())
            {

                bool foundUser = dbContext.Users.Any(u => u.UserID == user.UserID);
                bool foundRole = dbContext.Roles.Any(r => r.RoleID == role.RoleID);

                //Only select role and user if they are in DB
                role = foundRole ? dbContext.Roles
                    .Include(r => r.RoleUsers)
                    .FirstOrDefault(r => r.RoleID == role.RoleID)
                    : role;

                user = foundUser ? dbContext.Users
                    .Include(u => u.RoleUsers)
                    .FirstOrDefault(u => u.UserID == user.UserID)
                    : user;

                if (!foundRole) await AddRoleUser(role, user, role, dbContext, false);

                else if (!foundUser) await AddRoleUser(user, user, role, dbContext, false);

                else await AddRoleUser(user, user, role, dbContext, true);

                dbContext.SaveChanges();
            }
        }

        public static async Task RemoveRoleUsers(SocketGuildUser user, User dbUser)
        {
            using (NineBotContext dbContext = new NineBotContext())
            {
                bool foundUser = dbContext.Users.Any(u => u.UserID == dbUser.UserID);
                if (!foundUser)
                {
                    //TODO: Throw exception
                    return;
                }

                dbUser = dbContext.Users
                    .Include(u => u.RoleUsers)
                    .FirstOrDefault(u => u.UserID == dbUser.UserID);

                foreach (var roleUser in dbUser.RoleUsers.ToArray())
                {
                    bool roleFound = user.Roles.Any(r => r.Id == roleUser.RoleId);

                    if (roleFound) continue;

                    else dbUser.RoleUsers.Remove(roleUser);
                }

                await dbContext.SaveChangesAsync();
            }


        }
    }
}
