﻿using NineBot.Models;
using System.Collections.Generic;

namespace NineBot.Interfaces
{
    interface IRoleOrUser
    {

        ICollection<RoleUser> RoleUsers { get; set; }

    }

}
