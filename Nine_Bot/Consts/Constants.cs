﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NineBot.Consts
{
    class Constants
    {
        public static string[] PermissionsConsts = {"", //To start at index 1
                                        "Administrator",
                                        "CreateInstantInvite",
                                        "KickMembers",
                                        "BanMembers",
                                        "512",
                                        "ManageChannels",
                                        "ManageGuild",
                                        "AddReactions",
                                        "ViewAuditLog",
                                        "ViewChannel",
                                        "SendMessages",
                                        "ReadMessages",
                                        "SendTTSMessages",
                                        "ManageMessages",
                                        "EmbedLinks",
                                        "AttachFiles",
                                        "ReadMessageHistory",
                                        "MentionEveryone",
                                        "UseExternalEmojis",
                                        "Connect",
                                        "Speak",
                                        "MuteMembers",
                                        "DeafenMembers",
                                        "MoveMembers",
                                        "UseVoiceActivation",
                                        "PrioritySpeaker",
                                        "ChangeNickname",
                                        "ManageNicknames",
                                        "ManageRoles",
                                        "ManageWebhooks",
                                        "UseVAD",
                                        "ManageEmojis"
        };
        
    }
}
