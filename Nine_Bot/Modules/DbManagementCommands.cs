﻿using System.Threading.Tasks;
using Discord.Commands;
using NineBot.Db_logic;
using NineBot.Utils;
using NineBot.Models;


namespace NineBot.Modules
{
    public class DbManagementCommands : ModuleBase<SocketCommandContext>
    {

        [Command("updatedb", RunMode = RunMode.Async)]
        public async Task UpdateDb()
        {
            foreach (var user in Context.Guild.Users)
            {
                User dbUser = Utilties.DiscordUserToDbUser(user);
                await Db_abstraction.UpdateUser(dbUser, user);
            }

            foreach(var role in Context.Guild.Roles)
            {
                Role dbRole = Utilties.DiscordRoleToDbRole(role);
                await Db_abstraction.UpdateRole(dbRole, role);
            }
        }
    }
}
