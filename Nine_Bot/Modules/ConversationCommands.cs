﻿using System.Threading.Tasks;
using System.Collections.Generic;
using Discord;
using Discord.Commands;


namespace NineBot.Modules
{
    public class ConversationCommands: ModuleBase<SocketCommandContext>
    {
        //This api wrapper uses decorators to define commands
        [Command("echo")]
        public async Task EchoCommand([Remainder] string args)
            => await ReplyAsync(args);

        [Command("hello")]
        public async Task HelloCommand(IUser user = null) {
            user = user ?? Context.User;
            string msg = $"Hello {user.ToString()}";
            await ReplyAsync(msg);
        }
    }
}

