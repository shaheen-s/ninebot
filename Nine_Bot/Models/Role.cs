﻿using System;
using System.Collections.Generic;
using NineBot.Interfaces;


namespace NineBot.Models
{
    class Role: IRoleOrUser
    {

        public ulong RoleID { get; set; }

        public string Name { get; set; }

        public string Color { get; set; }

        public ICollection<RolePermission> RolePermissions { get; set; }

        public bool MentionableByAll { get; set; }

        public bool DisplayedSeperately { get; set; }

        public ICollection<RoleUser> RoleUsers { get; set; }

    }
}
