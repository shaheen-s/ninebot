﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NineBot.Models
{
    class Permission
    {
        public ulong PermissionID { get; set; }

        public string Name { get; set; }

        public ICollection<RolePermission> RolePermissions { get; set; }
    }
}
