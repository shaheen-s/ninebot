﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NineBot.Models
{
    class RolePermission
    {
        public ulong RoleId { get; set; }
        public Role Role { get; set; }

        public ulong PermissionId { get; set; }
        public Permission Permission { get; set; }
    }
}
