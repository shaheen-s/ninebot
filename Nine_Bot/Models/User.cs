﻿using System.Collections.Generic;
using NineBot.Interfaces;

namespace NineBot.Models
{
    class User: IRoleOrUser
    {

        public ulong UserID { get; set; }

        public string TagName { get; set; }

        public string TagID { get; set; }

        public ICollection<RoleUser> RoleUsers { get; set; }

        public int SentMessages { get; set; }

        public int Level { get; set; }

        public int Warnings { get; set; }
    }

}
