﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NineBot.Models
{
    class RoleUser
    {
        public ulong RoleId { get; set; }
        public Role Role { get; set; }

        public ulong UserId { get; set; }
        public User User { get; set; }

    }
}
