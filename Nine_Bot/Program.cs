﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using NineBot.Services;

namespace NineBot
{
    class Program
    {
        static void Main()
            => new Program().MainAsync().GetAwaiter().GetResult();
        
        public async Task MainAsync()
        {
            //"using" is used here for automatic garbage collection by defining a scope.
            // There is a dispose method to do the garbage collection.
            using (var services = ConfigureServices())
            {
                //Initialize the client
                var client = services.GetRequiredService<DiscordSocketClient>();
                client.Log += LogAsync;

                //Start it
                await client.LoginAsync(TokenType.Bot, Environment.GetEnvironmentVariable("NineBot_token"));
                await client.StartAsync();

                //Initialize commands
                await services.GetRequiredService<CommandHandlingService>().InitializeAsync();

                //Run this forever.
                await Task.Delay(-1);
            }
        }

        //Logging
        public Task LogAsync(LogMessage log)
        {
            Console.WriteLine(log.ToString());
            return Task.CompletedTask;
        }

        //Importing wanted services.
        private ServiceProvider ConfigureServices()
        {
            return new ServiceCollection()
                .AddSingleton<DiscordSocketClient>()
                .AddSingleton<CommandService>()
                .AddSingleton<CommandHandlingService>()
                .AddSingleton<HttpClient>()
                .BuildServiceProvider();
        }




    }
}
