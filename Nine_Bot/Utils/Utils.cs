﻿using System;
using System.Linq;
using System.Collections.Generic;
using NineBot.Consts;
using NineBot.Consts;
using NineBot.Models;


namespace NineBot.Utils
{
    class Utilties
    {
        public static User DiscordUserToDbUser(Discord.WebSocket.SocketGuildUser discordUser)
        {
            var user = new User
            {
                UserID = discordUser.Id,
                Level = 0,
                Warnings = 0,
                SentMessages = 0,
                TagName = discordUser.ToString().Split('#')[0],
                TagID = discordUser.ToString().Split('#')[1]
            };

            return user;
        }

        public static Role DiscordRoleToDbRole(Discord.WebSocket.SocketRole discordRole)
        {
            List<Permission> permissions = new List<Permission>();

            foreach (var permission in discordRole.Permissions.ToList())
            {
                permissions.Add(DiscordPermissionToDbPermission(permission));
            }

            string modifiedName = discordRole.Name;

            //remove @ sign if it exists in role(namely Everyone role)
            if (discordRole.Name.Contains('@')) modifiedName = modifiedName.Substring(1); 

            Role role = new Role
            {
                RoleID = discordRole.Id,
                Name = modifiedName,
                Color = discordRole.Color.ToString(),
                MentionableByAll = discordRole.IsMentionable,
                DisplayedSeperately = discordRole.IsHoisted,
            };

            return role;
        }

        public static Permission DiscordPermissionToDbPermission(Discord.GuildPermission permission)
        {
            Permission dbPerm = new Permission
            {
                Name = permission.ToString(),
                PermissionID = (ulong)Array.IndexOf(Constants.PermissionsConsts, permission.ToString())
            };


            return dbPerm;
        }
    }
}
