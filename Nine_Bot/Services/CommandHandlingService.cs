﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Discord;
using Discord.Commands;
using Discord.WebSocket;

namespace NineBot.Services
{
    public class CommandHandlingService
    {
        private readonly CommandService _commands;
        private readonly DiscordSocketClient _discord;
        private readonly IServiceProvider _services;

        public CommandHandlingService(IServiceProvider services)
        {
            _commands = services.GetRequiredService<CommandService>();
            _discord = services.GetRequiredService<DiscordSocketClient>();
            _services = services;

            //We add our defined function to define post command logic (mostly for logging)
            _commands.CommandExecuted += CommandExecutedAsync;
            //We hook our function to handle logic for recieving a message
            _discord.MessageReceived += MessageReceivedAsync;
        }

        public async Task InitializeAsync()
        {
            await _commands.AddModulesAsync(Assembly.GetEntryAssembly(), _services);
        }

        public async Task MessageReceivedAsync(SocketMessage rawMessage)
        {
            //ignore messages from system or non-users (bots)
            if (!(rawMessage is SocketUserMessage message)) return;
            if (message.Source != MessageSource.User) return;

            // this defines where the prefix ends
            var argPos = 0;

            //Only read messages that start with !
            if (!message.HasCharPrefix('!', ref argPos)) return;

            //Getting the context of where a command is said (server, channel, etc)
            var context = new SocketCommandContext(_discord, message);

            //Execute the command
            await _commands.ExecuteAsync(context, argPos, _services);
        }

        public async Task CommandExecutedAsync(Optional<CommandInfo> command, 
                                               ICommandContext context, IResult result)
        {
            //if (command.IsSpecified)
            //{
            //    //If command wasn't found, do not send an error.
            //    return;
            //}

            if (result.IsSuccess)
            {
                //If the command worked fine, there is no need for logging.
                return;
            }

            //Send error log
            string error = $"Error: {result}";
            await context.Channel.SendMessageAsync(error);
        }
    }
}